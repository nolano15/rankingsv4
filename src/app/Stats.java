package app;

import java.util.OptionalDouble;
import java.util.function.Supplier;
import java.util.stream.DoubleStream;

public class Stats {

    double mean, sd;

    public Stats(Supplier<DoubleStream> dbls) {
        this.mean = computeMean(dbls.get()).getAsDouble();
        this.sd = computeStdDev(dbls.get());
    }

    public static OptionalDouble computeMean(final DoubleStream dbls) {
    	return dbls.average();
    }
    
    private double computeVariance(final DoubleStream dbls) {
        return computeMean(dbls.map(x -> (x - mean)*(x - mean))).getAsDouble();
    }

    private double computeStdDev(final DoubleStream dbls) {
        return Math.sqrt(computeVariance(dbls));
    }
}