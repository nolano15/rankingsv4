package app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.stream.Stream;

import app.connections.*;
import app.results.ClusterResults;
import app.results.JsonResults;
import app.results.strategies.*;

public class Manager {

	public static void main(String args[]) throws IOException {
		if (args.length == 0) {
			System.out.println("Please enter a year");
			System.exit(0);
		}

		final String year = args[0];

		var teamsHolder = new TeamsHolder();

		Stream.of(
			new FPlusConnection(teamsHolder, year),
			new IsoConnection(teamsHolder, year),
			new TflConnection(teamsHolder, year),
			new PbuConnection(teamsHolder, year)
		).parallel().forEach(AbstractConnection::extractTableData);

		teamsHolder.checkTeamsLoaded();

		System.out.println("Calculating...");

		teamsHolder.computeScores();

		Stream.of(
			new CompositeResults(teamsHolder),
			new IsoHavocResults(teamsHolder),
			new IsoResults(teamsHolder),
			new HavocResults(teamsHolder)
		).parallel().map(ClusterResults::new)
		.forEachOrdered(results -> {
			try {
				syncGKeep(results);
			} catch (IOException ignored) {}
		});

		System.out.println("Done!");
	}

	/**
	 * Push new rankings to Google Keep, by passing as json string to python script
	 * 
	 * @param results implementation of rankings strategy
	 * @throws IOException
	 * 		if any problem running the python script
	 */
	private static void syncGKeep(JsonResults results) throws IOException {
		Process p = Runtime.getRuntime().exec(new String [] {
			"python3", "gkeep.py", results.getMode(),
			results.toJsonString()
		});

		// Print any errors that python outputs
		try(var err = new BufferedReader(new InputStreamReader(p.getErrorStream()))) {
			String s;
			while ((s = err.readLine()) != null) {
				System.out.println(s);
			}
		}
	}
}