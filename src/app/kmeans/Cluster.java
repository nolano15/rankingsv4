package app.kmeans;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.DoubleStream;

public class Cluster {

	private Set<CentroidAcceptor> nodes;
	private double centroid;

	public Cluster(double centroid) {
		this(new HashSet<>());
		this.centroid = centroid;
	}

	public Cluster(final Collection<CentroidAcceptor> nodes) {
		this.nodes = new HashSet<>(nodes);
	}

	DoubleStream getCentroidValues(NodeVisitor nodeVisitor) {
        return nodes.stream().mapToDouble(node -> node.getCentroidValue(nodeVisitor));
    }

	void addNode(CentroidAcceptor node) {
		nodes.add(node);
	}

	public Set<CentroidAcceptor> getNodes() {
		return nodes;
	}

	void setCentroid(double d) {
		this.centroid = d;
	}

	public double getCentroid() {
		return centroid;
	}
}
