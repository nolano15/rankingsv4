package app.kmeans;

public interface CentroidAcceptor {
    double getCentroidValue(NodeVisitor nodeVisitor);

    public double getSortingValue(NodeVisitor nodeVisitor);
}
