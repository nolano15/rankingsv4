package app.kmeans;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import app.Stats;

public class KMeans {

    // 5 nodes per cluster
    private static final int INIT_PART = 5;

    private List<CentroidAcceptor> nodes;
    private NodeVisitor nodeVisitor;

    public KMeans(List<CentroidAcceptor> nodes, NodeVisitor nodeVisitor) {
        this.nodes = nodes;
        this.nodeVisitor = nodeVisitor;
    }

    public Set<Cluster> getClusters() {
        final int K = nodes.size()/INIT_PART; // cluster formula

        // initialize k clusters, with initial partition of INIT_PART
        List<Cluster> clusters = IntStream.range(0, K)
            .mapToObj(i -> new Cluster(nodes.subList(i*INIT_PART, (i + 1)*INIT_PART)))
            .collect(Collectors.toList());
        
        clusters.parallelStream().forEach(this::computeCentroid);

        // filter out empty clusters
        return runAlgorithm(clusters).parallelStream().filter(cluster -> !cluster.getNodes().isEmpty())
            .collect(Collectors.toSet());
    }

    /**
     * K-Means algorithm assigns nodes to new clusters
     * and then makes recusrive call until the clusters do not change anymore
     * 
     * @param clusters initial clusters for this iteration
     * @return final clusters after the algorithm has finished
     */
    private List<Cluster> runAlgorithm(final List<Cluster> clusters) {
        // Generate k new clusters
        final List<Cluster> newClusters = createNewClusters(clusters);

        // if the clusters changed, make a recursive call
        return didClustersChange(clusters, newClusters) ? runAlgorithm(newClusters) : clusters;
    }

    /**
     * The initial clusters are used for comparing
     * and then nodes are assigned to new clusters
     * 
     * @param oldClusters initial clusters
     * @return newly created clusters
     */
    private List<Cluster> createNewClusters(final List<Cluster> oldClusters) {
        // Generate k new clusters
        // Initialize centroid to the previous cluster's value
        final List<Cluster> newClusters = IntStream.range(0, oldClusters.size())
            .mapToObj(i -> new Cluster(oldClusters.get(i).getCentroid()))
            .collect(Collectors.toList());
        
        // Assign each node to the nearest centroid
        nodes.parallelStream().forEach(node ->
            // adds node to index of nearest centroid
            newClusters.get(findNearestCentroid(oldClusters, node)).addNode(node));

        newClusters.parallelStream().forEach(this::computeCentroid);
        
        return newClusters;
    }

    /**
     * Compares centroid for each cluster
     * 
     * @param oldClusters initial clusters
     * @param newClusters newly assigned clusters
     * @return true if any centroids changed
     */
    private boolean didClustersChange(final List<Cluster> oldClusters, final List<Cluster> newClusters) {
        // for all k clusters
        // breaks loop as soon as an update is detected
        for(int i = 0; i < newClusters.size(); i++) {
            // Clusters being compared this iteration
            var newCluster = newClusters.get(i);
            var oldCluster = oldClusters.get(i);

            if(newCluster.getCentroid() != oldCluster.getCentroid()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Finds the ndx of the nearest centroid for a node
     * 
     * @param clusters initial clusters, with centroids to compare against
     * @param node node to calculate distance from cluster centroids
     * @return ndx of the nearest centroid
     */
    private int findNearestCentroid(final List<Cluster> clusters, CentroidAcceptor node) {
        int minNDX = -1;
        double dist, minDist = Double.POSITIVE_INFINITY;
        
        // find the closest centroid
        for(int i = 0; i < clusters.size(); i++) {
        	var cluster = clusters.get(i);
        	
        	// abs since no need to square
        	dist = Math.abs(node.getCentroidValue(nodeVisitor) - cluster.getCentroid());
        	
            if(dist < minDist) {
            	// update min vals
                minDist = dist;
                minNDX = i;
            }
        }    
        return minNDX;
    }

    private void computeCentroid(Cluster cluster) {
        Stats.computeMean(cluster.getCentroidValues(nodeVisitor)).ifPresent(cluster::setCentroid);
    }
}