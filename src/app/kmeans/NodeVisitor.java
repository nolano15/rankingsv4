package app.kmeans;

import app.Team;

public interface NodeVisitor {
    public abstract double visitTeam(Team team);
    abstract double sortTeams(Team team);
}
