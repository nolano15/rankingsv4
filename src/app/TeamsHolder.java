package app;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import app.kmeans.*;

public class TeamsHolder {

	private static final int TEAM_COUNT = 127; // FBS

	// in memory store
	private Set<Team> teams = ConcurrentHashMap.newKeySet(TEAM_COUNT);

	private Set<String> dataLoadErrors = ConcurrentHashMap.newKeySet();

	public TeamsHolder() throws IOException {
		try (BufferedReader in = new BufferedReader(new FileReader("teamNames.csv"))) {
			String line;
			System.out.println("Loading...");
			while ((line = in.readLine()) != null) {
				teams.add(new Team(line.split(",")));
			}
		}
	}

	public boolean submitTableValidationResults(Set<String> errors) {
		return dataLoadErrors.addAll(errors);
	}

	void checkTeamsLoaded() {
		if (!dataLoadErrors.isEmpty()) {
			System.out.println("Table validation error\nTeam data could not be loaded");
			System.out.println("Errors = " + dataLoadErrors);
			System.exit(0);
		}
	}

	public Optional<Team> findTeam(String teamName) {
		return teams.parallelStream().filter(team -> team.containsName(teamName)).findFirst();
	}

	void computeScores() {
		final var isoStats = new Stats(() -> teams.parallelStream().mapToDouble(team -> team.iso));
		final var havocStats = new Stats(() -> teams.parallelStream().mapToDouble(Team::getHavoc));

		teams.parallelStream().forEach(team -> {
			// explosiveness and havoc
			double off = (team.iso - isoStats.mean)/isoStats.sd;
			double def = (team.getHavoc() - havocStats.mean)/havocStats.sd;
	 
			// set score
			team.score = .6*off + .4*def;
		});
	}

	public Set<Cluster> getKMeansClusters(NodeVisitor nodeVisitor) {
		// encapsulation
		// initialize data for k-means by sorting
		// improves results and performance
		return new KMeans(teams.stream().sorted(Comparator.comparing(nodeVisitor::visitTeam).reversed())
			.collect(Collectors.toList()), nodeVisitor).getClusters();
	}
}