package app;

import java.util.Set;

import app.kmeans.CentroidAcceptor;
import app.kmeans.NodeVisitor;

public class Team implements CentroidAcceptor {
	
	private Set<String> nameSet;
	private String primaryName;
	public double fplus, iso, tfl, pbu, score;

	public Team(String... names) {
		this.nameSet = Set.of(names);
		this.primaryName = names[0];
	}

	boolean containsName(String teamName) {
		return nameSet.contains(teamName);
	}

	public double getScore() {
		return score;
	}

	@Override
	public double getCentroidValue(NodeVisitor nodeVisitor) {
		return nodeVisitor.visitTeam(this);
	}

	@Override
	public double getSortingValue(NodeVisitor nodeVisitor) {
		return nodeVisitor.sortTeams(this);
	}

	public double getHavoc() {
		return tfl + pbu;
	}

	public String getPrimaryName() {
		return primaryName;
	}

	@Override
	public String toString() {
		return "{\"name\": \"" + primaryName + "\"}";
	}
}