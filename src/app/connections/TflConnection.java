package app.connections;

import java.io.IOException;
import java.util.stream.Stream;

import app.Team;
import app.TeamsHolder;

public class TflConnection extends AbstractStatConnection {

    private static final int category = 21;
    private static final String tableName = "Tackles For Loss";

    private static final Stream<String> COLUMNS = Stream.of("tfl highlight", "tfl-yard", "tfl-g");

    public TflConnection(TeamsHolder teamsHolder, String year) throws IOException {
        super(teamsHolder, COLUMNS, tableName, year, category);
    }

    @Override
    public void updateTeam(Team team) {
        urlRead();
        urlRead();
        urlRead();

        team.tfl = extractTableDouble();
    }
}