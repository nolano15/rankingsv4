package app.connections;

import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import app.TeamsHolder;

public abstract class AbstractStatConnection extends AbstractConnection {

    private static final String urlTemplate = "http://www.cfbstats.com/%s/leader/national/team/offense/split01/category%d/sort01.html";
    
    private final Stream<String> COMMON_COLUMNS = Stream.of("rank", "team-name", "g");
    private Stream<String> specificColumns;

    public AbstractStatConnection(TeamsHolder teamsHolder, Stream<String> specificColumns, String tableName, String year, int category) throws IOException {
        super(teamsHolder, urlTemplate, year, category);
        this.specificColumns = specificColumns;

        teamsHolder.submitTableValidationResults(validateTableName(tableName));
    }

    private Set<String> validateTableName(String tableName) {
        // find page title
        while(!urlRead().contains("id=\"pageTitle\""));

        // check exact match
        if(urlRead().equals(tableName + " - All Games")) {
            return Set.of();
        } else {
            return Set.of("TableName: " + tableName);
        }
    }

    @Override
    protected Set<String> validateColumnHeaders() {
        urlRead(); // read opening tr element

        Set<String> errors = Stream.concat(COMMON_COLUMNS, specificColumns)
            .filter(columnName -> !urlRead().contains("class=\"" + columnName + "\""))
            .map(columnName -> "ColumnName: " + columnName)
            .collect(Collectors.toSet());

        readRow(); // finish reading the current row

        return errors;
    }
}