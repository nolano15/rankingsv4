package app.connections;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import app.Team;
import app.TeamsHolder;

public class FPlusConnection extends AbstractConnection {

    private static final String urlTemplate = "https://www.footballoutsiders.com/stats/ncaa/fplus/%s";

    public FPlusConnection(TeamsHolder teamsHolder, String year) throws IOException {
        super(teamsHolder, urlTemplate, year);
    }

    @Override
    protected Set<String> validateColumnHeaders() {
        // I trust this website and use it often personally. It wont surprise me.
        // so just read/skip the header row here
        readRow();
        return new HashSet<>(); // no errors
    }

    @Override
    public void updateTeam(Team team) {
        urlRead();
        team.fplus = extractTableDouble();
    }
}