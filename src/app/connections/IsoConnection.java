package app.connections;

import java.io.IOException;
import java.util.Set;
import java.util.stream.Stream;

import app.Team;
import app.TeamsHolder;

public class IsoConnection extends AbstractStatConnection {

    private static final int category = 30;
    private static final String tableName = "Long Scrimmage Plays";

    private static final Stream<String> COLUMNS = Stream.of("long-play-10-plus highlight", "long-play-20-plus", "long-play-30-plus");

    public IsoConnection(TeamsHolder teamsHolder, String year) throws IOException {
        super(teamsHolder, COLUMNS, tableName, year, category);
    }

    @Override
    protected Set<String> validateColumnHeaders() {
        Set<String> errors = super.validateColumnHeaders();

        readRow(); // read the extra header row for this table

        return errors;
    }

    @Override
    public void updateTeam(Team team) {
        double games = extractTableDouble();

        urlRead();

        // 20+ yd plays and half of the 30+ yd plays
        // because 30+ is already counted in the 20+ column
        team.iso = (extractTableDouble() + .5*extractTableDouble())/games;
    }
}