package app.connections;

import java.io.IOException;
import java.util.stream.Stream;

import app.Team;
import app.TeamsHolder;

public class PbuConnection extends AbstractStatConnection {

    private static final int category = 23;
    private static final String tableName = "Passes Defended";

    private static final Stream<String> COLUMNS = Stream.of("int-ret", "pass-broken-up", "pass-defended highlight", "pass-defended-g");

    public PbuConnection(TeamsHolder teamsHolder, String year) throws IOException {
        super(teamsHolder, COLUMNS, tableName, year, category);
    }

    @Override
    public void updateTeam(Team team) {
        urlRead();
        urlRead();
        urlRead();
        urlRead();

        team.pbu = extractTableDouble();
    }
}