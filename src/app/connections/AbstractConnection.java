package app.connections;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import app.Team;
import app.TeamsHolder;

public abstract class AbstractConnection {

    private TeamsHolder teamsHolder;
    private BufferedReader urlConnReader;

    public AbstractConnection(TeamsHolder teamsHolder, String urlString, Object... formattingArgs) throws IOException {
        this.teamsHolder = teamsHolder;

        var url = new URL(String.format(urlString, formattingArgs));
        this.urlConnReader = new BufferedReader(new InputStreamReader(url.openConnection().getInputStream()));
    }

    /**
     * Each connection will verify that the columns to pull data from are correct
     * 
     * @return errors, if any
     */
    protected abstract Set<String> validateColumnHeaders();

    /**
     * Each connection will implement its strategy for updating Teams
     * 
     * @param team Team class to update
     */
    protected abstract void updateTeam(Team team);

	public void extractTableData() {
		// find table body
        while(!urlRead().matches("<table.*>"));

        teamsHolder.submitTableValidationResults(validateColumnHeaders());

        // while there are more rows
        while(urlRead().trim().matches("<tr.*>")) {
            processRow();
        }
    }

	private void processRow() {
        
        updateTeam(findTeam());

        // finish reading table row
        readRow();
    }

    protected void readRow() {
        while(!"</tr>".equals(urlRead().trim()));
    }

    private Team findTeam() {
        // read first cell that I don't need
        urlRead();

        String teamName = extractTableString()
            // needed for Texas A&M
            .replace("&amp;", "&");

        return teamsHolder.findTeam(teamName).get();
    }

    protected double extractTableDouble() {
        return Double.parseDouble(extractTableString());
    }

    private String extractTableString() {
        // ? on each * to suppress greedy behavior
        Matcher matcher = Pattern.compile("<td.*?>(?:<a href=.*?>)?(?<data>.*?)(?:</a>)?</td>").matcher(urlRead());
        matcher.find();
        return matcher.group("data");
    }

    protected String urlRead() {
        try {
            return urlConnReader.readLine();
        } catch (IOException e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
            return null;
        }
    }
}