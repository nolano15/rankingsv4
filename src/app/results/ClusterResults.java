package app.results;

import java.util.Comparator;
import java.util.Set;
import java.util.stream.Collectors;

import app.kmeans.Cluster;
import app.results.strategies.ResultStrategy;

public class ClusterResults implements JsonResults {

    private Set<Cluster> clusters;
    private ResultStrategy resultStrategy;
 
    public ClusterResults(ResultStrategy resultStrategy) {
        this.clusters = resultStrategy.generateClusters();

        this.resultStrategy = resultStrategy;
    }

    @Override
    public String getMode() {
        return resultStrategy.mode;
    }

    @Override
    public String toJsonString() {
        // sort clusters by centroid
        return clusters.stream().sorted(Comparator.comparing(Cluster::getCentroid).reversed())
            .limit(resultStrategy.clusterLimit)
            .map(this::toJsonString)
            .collect(Collectors.toList()).toString();
    }

    /**
	 * Sorts within a cluster and returns nodes in JSON format
	 * 
	 * @param cluster cluster to convert to JSON string
	 * @return json formatted string of the nodes within this cluster
	 */
	private String toJsonString(Cluster cluster) {
		return "{\"teams\": " +
			cluster.getNodes().stream().sorted(Comparator.comparing(resultStrategy::sortNode).reversed())
				.collect(Collectors.toList()) + "}";
	}
}
