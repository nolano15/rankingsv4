package app.results.strategies;

import java.util.Set;

import app.Team;
import app.TeamsHolder;
import app.kmeans.CentroidAcceptor;
import app.kmeans.Cluster;
import app.kmeans.NodeVisitor;

public class HavocResults extends ResultStrategy implements NodeVisitor {

    private static final String MODE = "--havoc";

    public HavocResults(TeamsHolder teamsHolder) {
        super(teamsHolder, MODE, 2);
    }

    @Override
    public double visitTeam(Team team) {
        return team.getHavoc();
    }

    @Override
    public double sortTeams(Team team) {
        return team.getHavoc();
    }

    @Override
    public Set<Cluster> generateClusters() {
        return teamsHolder.getKMeansClusters(this);
    }

    @Override
    public double sortNode(CentroidAcceptor node) {
        return node.getSortingValue(this);
    }
}
