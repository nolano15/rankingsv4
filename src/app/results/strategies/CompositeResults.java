package app.results.strategies;

import java.util.Set;

import app.Team;
import app.TeamsHolder;
import app.kmeans.CentroidAcceptor;
import app.kmeans.Cluster;
import app.kmeans.NodeVisitor;

public class CompositeResults extends ResultStrategy implements NodeVisitor {

    private static final String MODE = "--composite";

    public CompositeResults(TeamsHolder teamsHolder) {
        super(teamsHolder, MODE, 5);
    }

    @Override
    public double visitTeam(Team team) {
        return team.fplus;
    }

    @Override
    public double sortTeams(Team team) {
        return team.getScore();
    }

    @Override
    public Set<Cluster> generateClusters() {
        return teamsHolder.getKMeansClusters(this);
    }

    @Override
    public double sortNode(CentroidAcceptor node) {
        return node.getSortingValue(this);
    }
}
