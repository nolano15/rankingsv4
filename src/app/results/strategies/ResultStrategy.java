package app.results.strategies;

import java.util.Set;

import app.TeamsHolder;
import app.kmeans.CentroidAcceptor;
import app.kmeans.Cluster;

public abstract class ResultStrategy {

    protected TeamsHolder teamsHolder;
    
    public String mode;
    public int clusterLimit;

    public ResultStrategy(TeamsHolder teamsHolder, String mode, int clusterLimit) {
        this.teamsHolder = teamsHolder;

        this.mode = mode;
        this.clusterLimit = clusterLimit;
    }

    public abstract Set<Cluster> generateClusters();

    /**
     * Returns value to sort a node by based off its visitor implementation
     * 
     * @param node given node to get sorting value
     * @return double value to use for sorting
     */
    public abstract double sortNode(CentroidAcceptor node);
}
