package app.results;

public interface JsonResults {
    public String getMode();
    public String toJsonString();
}
