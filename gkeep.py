import gkeepapi, os, sys, json

# id's for gnotes
id_dict = {
    '--composite': '1JFmVEeuZhWt4zvI31Js9YsznZRUt9sU7-M69JS3ypGCF-oHuvUgbin4t_PWOGd0',
    '--isoHavoc': '1BQl_AproWLcHhuuU5NgX92xwQ4DH4dRQWr7OFpgLORKZzvaPeWZOnQpu3HJSSsZJgcBy',
    '--iso': '1NT6Sfts0eidGlcZVRf-3_YJ-Txb3Eb809OlJ6aFbGYcmmK7GlkLyOVqsGuPqix6R7JbK',
    '--havoc': '1SoMsak5RTUIWO4rRXikEXHFNmPQSI_psOmS6_p-RPZmjsvi-aH5yQpoRp6KWsLRf8VSo'
}

NOTE_ID = id_dict.get(sys.argv[1])

# input validation
if NOTE_ID is None:
    print('Unsupported rankings mode\nExample: --clusters')
    sys.exit()

clusters = json.loads(sys.argv[2])

keep = gkeepapi.Keep()

print('Logging into Google...')
# IMPORTANT!!
# I had to manually disable the gkeepapi's sync of reminders
# otherwise there were 404 errors
keep.login('nolano14@gmail.com', os.environ.get("NOLAN_GOOGLE_APP_PW", None))

gnote = keep.get(NOTE_ID)

gnote.text = ''
i = 1

for cluster in clusters:
    for j, team in enumerate(cluster['teams'], start=i):
        gnote.text += '{}) {}\n'.format(str(j), team['name'])
        i += 1
    gnote.text += '\n'

keep.sync()