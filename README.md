## Rankings v4

### Components
1. Web Scraper that retrieves statistics from web pages.
2. k-Means Data Clustering algorithm. Within each cluster teams are sorted by different metrics.
3. Implements Visitor design pattern, so that the same k-Means algorithm can generate clusters from different centroid values.
4. Python script that syncs results to Google Keep using an unoffical API